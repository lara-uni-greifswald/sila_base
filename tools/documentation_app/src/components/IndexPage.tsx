import React from "react";
import ReactMarkdown from "react-markdown";
import markdown from "../documentation/mainpage/mainpage.md";
import Utilities from "../services/Utilities";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import quickstart from "../documentation/quickstart/quickstart.md";
import ecosystem from "../documentation/ecosystem/ecosystem.md";
import guides from "../documentation/guides/guides.md";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        markdown: {
            width: "68%",
        },

    })
);

const IndexPage: React.FunctionComponent = () => {
    const classes = useStyles();
    return (
        <>
            {/* Just transform the links of the features with the Utilities.transformLinkUri*/}
            <ReactMarkdown
                renderers={Utilities.MDRenderers}
                transformLinkUri={Utilities.transformLinkUri}
                source={markdown}
                className={classes.markdown}
            />
        </>
    );
}

const QuickstartPage: React.FunctionComponent = () => {
    const classes = useStyles();
    return (
        <>
            {/* Just transform the links of the features with the Utilities.transformLinkUri*/}
            <ReactMarkdown
                renderers={Utilities.MDRenderers}
                transformLinkUri={Utilities.transformLinkUri}
                source={quickstart}
                className={classes.markdown}
            />
        </>
    );
}

const EcosystemPage: React.FunctionComponent = () => {
    const classes = useStyles();
    return (
        <>
            {/* Just transform the links of the features with the Utilities.transformLinkUri*/}
            <ReactMarkdown
                renderers={Utilities.MDRenderers}
                transformLinkUri={Utilities.transformLinkUri}
                source={ecosystem}
                className={classes.markdown}
            />
        </>
    );
}

const GuidesPage: React.FunctionComponent = () => {
    const classes = useStyles();
    return (
        <>
            {/* Just transform the links of the features with the Utilities.transformLinkUri*/}
            <ReactMarkdown
                renderers={Utilities.MDRenderers}
                transformLinkUri={Utilities.transformLinkUri}
                source={guides}
                className={classes.markdown}
            />
        </>
    );
}

export {QuickstartPage, GuidesPage, EcosystemPage};

export default IndexPage;
